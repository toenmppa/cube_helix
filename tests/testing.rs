use cube_helix::CubeHelix;

use cube_helix::color;

extern crate cube_helix;
//use cube_helix::*;

#[test]
fn struct_defaults_accessible() {
    let ch: CubeHelix = Default::default();
    assert_eq!(ch.gamma, 1.0);
    assert_eq!(ch.start, 0.5);
    assert_eq!(ch.rotations, -1.5);
    assert_eq!(ch.saturation, 1.0);
    assert_eq!(ch.min, 0.0);
    assert_eq!(ch.max, 1.0);
}

#[test]
fn get_color() {
    let ch: CubeHelix = Default::default();
    let color: (u8, u8, u8) = ch.get_color(0.5);
    assert_eq!(color.0, 160);
    assert_eq!(color.1, 121);
    assert_eq!(color.2, 73);
}

#[test]
fn function_call_for_getting_color() {
    let color: (u8, u8, u8) = color(1.0, 0.5, -1.5, 1.0, 0.0, 1.0, 0.5);
    assert_eq!(color.0, 160);
    assert_eq!(color.1, 121);
    assert_eq!(color.2, 73);
}

#[test]
fn test_struct_debug_printing() {
    let ch: CubeHelix = Default::default();
    println!("{:?}", ch);
}
